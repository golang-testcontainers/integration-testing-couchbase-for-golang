package couchbase

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"testing"
	"time"
)

func TestCouchbaseCluster(t *testing.T) {
	//given
	ctx := context.Background()

	couchbaseContainer, _ := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: testcontainers.ContainerRequest{
			Image:        "mustafaonuraydin/couchbase-testcontainer:6.5.1",
			ExposedPorts: []string{"8091:8091/tcp", "8093:8093/tcp", "11210:11210/tcp"},
			WaitingFor:   wait.ForLog("couchbase-dev started").WithStartupTimeout(45 * time.Second),
			Env:          map[string]string{"USERNAME": "User", "PASSWORD": "password", "BUCKET_NAME": "Product"},
		},
		Started: true,
	})

	defer couchbaseContainer.Terminate(ctx)

	//when
	cluster := Cluster()

	//then
	buckets, _ := cluster.Buckets().GetAllBuckets(nil)
	_, isBucketExist := buckets["Product"]
	assert.Equal(t, isBucketExist, true)
}
