package couchbase

import (
	"github.com/couchbase/gocb/v2"
	"sync"
)

var once sync.Once
var cluster *gocb.Cluster

func Cluster() *gocb.Cluster {
	once.Do(func() {
		cb, clusterError := gocb.Connect("localhost", gocb.ClusterOptions{
			Username: "User",
			Password: "password",
		})

		if clusterError != nil {
			panic(clusterError)
		}

		cb.Bucket("Product")

		cluster = cb
	})
	return cluster
}


