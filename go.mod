module integration-testing-couchbase-for-golang

go 1.14

require (
	github.com/couchbase/gocb/v2 v2.1.8
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
	github.com/testcontainers/testcontainers-go v0.9.0
)
