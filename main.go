package main

import 	(log "github.com/sirupsen/logrus")

func main() {
	defer recoverPanic()
}

func recoverPanic() {
	if rec := recover(); rec != nil {
		err := rec.(error)
		log.Error(err.Error())
	}
}